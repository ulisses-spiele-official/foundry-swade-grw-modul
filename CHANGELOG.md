### 1.3.1
Dieses Update dient in erster Linie der Kompatibilität mit Foundry v10 und setzt mindestens Foundry v10 (Build 286) voraus.

Alle Kompendien mit Ausnahme des Journal-Kompendium auf die neuen Datenstrukturen aktualisiert. Im Journal-Kompendium wurden bisher nur rudimentäre Anpassungen vorgenommen um die Nutzbarkeit zu verbessern (Thumbnails entfernt). Das Journal-Kompendium basiert weiterhin auf **CompendiumFolders** und kann sowohl mit **CompendiumFolders** oder der **SWADE Compendium TOC** genutzt werden. Für den Wechsel der Ansichten steht in der Kompendium-Liste im Kontext-Menü des Kompendium (Rechtsklick) der Eintrag **Toggle use Compendium TOC** zur Verfügung.

Sonstige Änderungen:

- FIX: Zuviel entfernte Bilder im GRW-Kompendium wiederhergestellt (Thx @Tommycore)

### 1.3.0
Dieses Update dient in erster Linie der Kompatibilität mit Foundry v10 und setzt mindestens Foundry v10 (Build 286) voraus.

Alle Kompendien mit Ausnahme des Journal-Kompendium auf die neuen Datenstrukturen aktualisiert. Im Journal-Kompendium wurden bisher nur rudimentäre Anpassungen vorgenommen um die Nutzbarkeit zu verbessern (Thumbnails entfernt). Das Journal-Kompendium basiert weiterhin auf **CompendiumFolders** und kann sowohl mit **CompendiumFolders** oder der **SWADE Compendium TOC** genutzt werden. Für den Wechsel der Ansichten steht in der Kompendium-Liste im Kontext-Menü des Kompendium (Rechtsklick) der Eintrag **Toggle use Compendium TOC** zur Verfügung.

Sonstige Änderungen:

- FIX: CSS Stile korrigiert, damit Item-Links wieder wie gewünscht angezeigt werden.
- FIX: Problem beseitigt, aufgrund dessen der Willkommensbildschirm nicht mehr angezeigt werden konnte.
- FIX: Problem behoben, aufgrund dessen es beim Drücken von [ENTER] im Dialog des Furchttabellen Würfelmakro zu einem Fehler kam. Das Drücken von [ENTER] löst jetzt (wie erwartbar) das Würfeln aus.
- FIX: Die Bezeichnung **STARTMÄCHTE** in den Arkanen Hintergründen korrigiert (Journal und Talente)
- FIX: Die Bezeichnung **starke Handicaps** in **schwere Handicaps** abgeändert (Journal)
- REFACTOR: Code des Furchttabellen Würfelmakro für bessere Wartbarkeit als Modulfunktion umgesetzt und Makro auf Funktionsaufruf umgestellt.
- REFACTOR: Das Makro für die Umstellung der Systemeinstellungen auf die deutschen Inhalte durch einen Dialog in den Moduleinstellungen ersetzt.
- Diverse interne Überarbeitungen


### 1.2.6
Wartungsupdate wegen Änderung diverser Webadressen. Keine inhaltlichen Änderungen.

### 1.2.5
- Fehlenden Paragraphen eingefügt.
- Mehrere Falsch benannte JournalEinträge richtig benannt und Verlinkungen gefixt.
- Kompatibilität für prä-v9 Versionen endgültig gekappt.

### 1.2.4
- Upgrade für Foundry v9 und Swade 1.0.2.
- Aktionskarten als Kartenkompendium erstellt.
- Furchttabellen-Macro repariert.
- Eine Menge Tippfehler und falsche Einträge gradegebogen (Danke an KaiderWeise).
- Natürliche Nahkampfwaffen als Items angelegt (Danke an Braincat und Lestat).

### 1.1.2
- Name vom Karo König korrigiert.
- Problem mit Anzeigenamen des Modulauthors auf FoundryHub behoben.
- Link zu diesem Changelog in's Manifest eingefügt.

### 1.1.1
Offizielle Freigabe

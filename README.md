# Willkommen bei der Savage Worlds Adventure Edition

# Inhaltsverzeichnis
1. [Einleitung](#einleitung)
2. [Modul kaufen](#kauf-des-moduls)
3. [Installation](#installation)
4. [Inhaltsübersicht](#inhaltsübersicht)
    1. [Regel-Kompendium](#regel-kompendium)
    2. [Akteur-Kompendien](#akteur-kompendien)
    3. [Item-Kompendien](#item-kompendien)
    4. [Würfeltabellen](#würfeltabellen)
5. [Makros](#makros)
6. [Moduleinstellungen](#moduleinstellungen)
7. [Andere Module](#andere-module)
8. [FAQ](#faq)

# Einleitung

Willkommen zum ersten offiziellen deutschen *Savage Worlds* Premium-Inhaltspaket für Foundry VTT. Darin findest du alle Inhalte aus dem Grundregelwerk (GRW) der Savage Worlds Abenteuer Edition (SWADE).  Darüber hinaus findest du alle Grafiken aus dem Grundregelwerk sowie mehr als 200 zusätzliche Grafiken. Viele andere Goodies aus den Grundregeln sind ebenfalls enthalten, wie zum Beispiel automatisierte Würfeltabellen, einsatzbereite Gegenstände, Waffen mit vorkonfigurierten Aktionen und tokenisierte Akteure und Fahrzeuge.

Wir hoffen, dass dir dieses Herzenswerk gefällt und dass deine Spiele rasant, spannend und lustig werden!

# Kauf des Moduls

## Lizenzschlüssel für das Modul kaufen

Du kannst einen Lizenzschlüssel und die begleitende Dokumentation für das Savage Worlds Abenteuer Edition Modul direkt im F-Shop kaufen: https://www.f-shop.de/detail/index/sArticle/2600

## Aktiviere das Modul in Foundry

Um deine Kopie des Moduls zu aktivieren, melde dich bei foundryvtt.com an und gib deinen Lizenzschlüssel in die Premium-Content-Oberfläche ein:

![Lizenzschlüssel aktivieren](img/activate.png) 

(1) Log dich ein und gehe auf dein Profil.

(2) Wähle den Bereich Purchased Content.

(3) Gib den Lizenz-Schlüssel ein.

(4) Bestätige mit Activate Content.

# Installation

## Modul herunterladen

Nachdem du das Modul erworben hast, kannst du es über die integrierte Installationsmodulschnittstelle von Foundry herunterladen.

![Modul installieren](img/modules-screen.png)

(1) Wechsel im Hauptmenü von Foundry zur Registerkarte **Add-on-Module**.

(2) Klicke auf die Schaltfläche **Modul installieren**.

Suche in der Benutzeroberfläche des Installationsmoduls nach dem Savage Worlds Abenteuer Edition Grundregelwerk-Modul (leicht zu finden unter der Paketkategorie „Premium-Inhalt“) und installiere es.

## Lade eine bestehende Welt oder erstelle eine neue

Dieses Modul ist mit bestehenden Welten kompatibel (solange sie eine ausreichend aktuelle SWADE-Systemversion nutzen). Da die Automatisierung allerdings vielfach auf konkrete Namen zurückgreift, kann es bei der Verwendung von gemischten deutsch/englischen Inhalten zu Problemen kommen.

Wenn es zu Schwierigkeiten kommen sollte, erstelle sicherheitshalber eine neue Welt für dein Spiel.

## Aktiviere das Modul

![Spieleinstellungen öffnen](img/game-settings.png)

(1) Wechsel zur Seitenleiste **Einstellungen** und klicke oben rechts auf das Zahnradsymbol.

(2) Klicke auf **Module verwalten**.

![Modul aktivieren](img/module-management.png)

(1) Klicke auf die Kontrollkästchen neben dem **_Savage Worlds_ Adventure Edition Grundregelwerk**, **libWrapper** und **Compendium Folders**.

(2) Wenn eine Meldung angezeigt wird, die dich auffordert, die Abhängigkeit "Compendium Folders" zu aktivieren, klicke auf Ja.

(3) Klicke auf die Schaltfläche Module aktualisieren

## Lese den "Willkommensbildschirm"

Wenn das Modul erfolgreich aktiviert wurde, wird der Willkommensbildschirm angezeigt. _Um das Beste aus diesem Modul herauszuholen, lies diesen unbedingt vollständig durch._

![Willkommensbildschirm](img/welcome.png)

Wenn du den Willkommensbildschirm nicht bei jedem Start sehen möchtest, klicke auf das Kontrollkästchen unten, um ihn zu deaktivieren.

## Schau dir deine schönen neuen offiziellen SWADE-Inhalte an!

Navigiere zur Seitenleiste Kompendienpakete, indem Sie auf das Buchsymbol klickst, das sich direkt links neben dem Zahnradsymbol befindet (oben rechts).

# Inhaltsübersicht

Die folgenden Kompendien sind innerhalb des Moduls mit offiziellen Inhalten verfügbar, um deine _Savage Worlds_-Spiele zum Leben zu erwecken.

Hinweis: Versuche, den Inhalt nach Möglichkeit in Kompendien zu belassen. Der Import von Hunderten oder Tausenden von nicht benötigten Akteuren, Gegenständen oder Journaleinträgen in deiner Welt kann sich negativ auf die Leistung von Foundry auswirken.

## Regel-Kompendium

Die Regeln von Savage Worlds sind in die gleichen Kapitel unterteilt wie das gedruckte Buch. Wenn du den Titel des gesuchten Artikels kennst, verwende die Suche oben in der Seitenleiste des Kompendiums, damit du nicht mehrere Ordner durchsuchen musst.

Die Regeln sind verlinkt, d. h. durch Klicken auf den rot unterstrichenen Text in jedem Artikel werden dir detailliertere Informationen zu dem jeweiligen Thema angezeigt. Einige davon sind Zwischenüberschriften in einem längeren Eintrag, daher musst du möglicherweise nach unten scrollen und den Artikel durchlesen und zu finden, wonach du suchst.

Hier findest du außerdem das deutsche **Aktionskartendeck**.

## Akteur-Kompendien

### Bestiarium

Dieses Kompendium enthält den gesamten Bestiarium-Abschnitt des Savage Worlds Grundregelwerks mit vollständigen Spielwerten, Beschreibungen und Grafiken – einschließlich **tokenisierter Akteure und nie zuvor gesehener Grafiken**!

### Fahrzeuge

Du findest auch alle Fahrzeuge, die im SWADE-GRW vorgestellt werden, mit vollständigen Spielwerten sowie der richtigen Bewaffnung. Zu den Fahrzeugen gehören Grafiken und entsprechend große, tokenisierte Versionen.

## Item-Kompendien

Die Gegenstandskompendien enthalten alle Fertigkeiten, Handicaps, Talente, Ausrüstung, Mächte, Völker und Eigenarten, sowie die Spezialfähigkeiten Bestiarium) aus dem SWADE-GRW, komplett mit vollständig verlinkten Beschreibungen.

**Hinweis:** Aufgrund dessen wie Foundry „unter der Haube“ aufgebaut ist, finden sich in den Gegenstandskompendien einige Dinge, die in Savage Worlds traditionell nicht als Gegenstände angesehen werden, wie Kräfte, Handicaps usw. trotzdem als Item (engl. für Gegenstand) angelegt. Grundsätzlich wird alles nach der Logik von Foundry als Gegenstand kategorisiert, wenn es auf dem Charakterbogen abgelegt oder manuell hinzugefügt werden kann.

Für Waffen sind vorkonfigurierte Chat-Kartenaktionen enthalten, um den Spielfluss zu erleichtern. 

## Würfeltabellen 

9 Stück enthalten: Persönlichkeiten von Verbündeten, Dynamischer Rückschlag, Schlachteffekte, Kreativer Kampf, Verletzungstabelle, Außer Kontrolle, Kritische Fahrzeugtreffer, Reaktionstabelle, Furchttabelle

# Makros

## Furchttabelle

Dies ist ein einfaches Makro, das den Benutzer zur Angabe eines Angstmalus auffordert, bevor er auf der Tabelle würfelt.

# Moduleinstellungen

## Spieleinstellungen für deutsche Inhalte konfigurieren

Über diesen Dialog können die Spieleinstellungen auf die deutschen Inhalte und dieses Modul umgestellt werden. Bitte beachte, dass dabei von dir vorher getätigte Einstellungen überschrieben werden könnten!

## Willkommensbildschirm beim Start anzeigen

Legt fest, ob der Willkommensbildschirm von Savage Worlds angezeigt wird, wenn sich ein Benutzer bei der Welt anmeldet.

# Andere Module

## Erforderliche Module für reibunglose Funktion des GRW Moduls:

### Compendium Folders

Dieses Modul wird als Abhängigkeit installiert, da es verwendet wird, um die _Savage Worlds_-Kompendienpakete in Ordnern zu organisieren.

https://foundryvtt.com/packages/compendium-folders/

### libWrapper

Um richtig zu funktionieren, braucht Compendium Folders libWrapper.

https://foundryvtt.com/packages/lib-wrapper/

## Empfohlene Module

### Dice So Nice!

Dieses Modul ermöglicht 3D-Würfel und vor allem 3D-Bennies. Versuch es!

https://foundryvtt.com/packages/dice-so-nice/

### PopOut!

Dieses Modul ist nicht erforderlich, aber deine Spieler könnten sich über die Möglichkeit freuen, Journal Einträge für Regeln in einem separaten Fenster anzuzeigen. Ein weiterer Bonus ist, dass du mit STRG+F innerhalb des Popup-Fensters nach Text suchen kannst, was in der Foundry-Ansicht nicht möglich ist.

https://foundryvtt.com/packages/popout/

# FAQ

## Wie erhalte ich Hilfe zu meinen gekauften Inhalten? 

Für Kundensupport im Zusammenhang mit gekauften Inhalten nutze bitte die folgenden Kanäle: 

Bei Problemen mit dem Kauf des Inhalts oder anderen Problemen mit dem F-Shop Webshop sende bitte eine E-Mail an feedback@ulisses-spiele.de 

Anweisungen zur Aktivierung oder Installation von gekauften Inhalten findest du im folgenden FAQ (https://foundryvtt.com/article/faq/#premium-content). Wenn bei diesem Vorgang Probleme auftreten, sende bitte eine Support-Anfrage über das Kontaktformular (https://foundryvtt.com/contact-us/). 

Bei Problemen oder Feedback zu den Inhalten selbst verwende bitte den #swade-vtt Kanal im Ulisses-Discord.

## Wenn ich in einem Kompendium nachsehe, werden keine oder kaum Elemente angezeigt.

Bitte überprüfe, ob die Suchleiste oben im Kompendium leer ist. Normalerweise wird dieses Problem durch übrig gebliebene Wörter im Suchfeld oben in der Ansicht Kompendium verursacht. Durch das Leeren des Suchfeldes werden alle Einträge angezeigt. 

## Wie kann ich all diese coolen Grafiken in meinen eigenen Homebrew-Inhalten verwenden?

Alle Grafik Assets in WEBP-optimiertem Format finden sich im Ordner modules/swade-core-rules-ger/assets/art. Diese sind nur für den privsten Gebrauch bestimmt.
